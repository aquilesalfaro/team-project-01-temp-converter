//
//  ViewController.h
//  TeamProject01-TempConverter
//
//  Created by user on 9/27/17.
//  Copyright © 2017 user. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface ViewController : UIViewController

-(void)calculateConversion:(int)selection;
-(void)setMinMax:(float)min : (float)max;





@end

