//
//  ViewController.m
//  TeamProject01-TempConverter
//
//  Created by user on 9/27/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *sgmSelector;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderValue;
@property (weak, nonatomic) IBOutlet UISlider *sldTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblConversionResult;
@property (weak, nonatomic) IBOutlet UILabel *lblMin;
@property (weak, nonatomic) IBOutlet UILabel *lblMax;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setMinMax:32.00 :212.00];
    
    [self calculateConversion:0];
    
}

- (IBAction)sgmSegmentChange:(id)sender {
    if(_sgmSelector.selectedSegmentIndex == 0){
        
        [self setMinMax:32.00 :212.00];
        
        [self calculateConversion:0];
        
    }else if(_sgmSelector.selectedSegmentIndex == 1){
        
        [self setMinMax:0.00 :100.00];
        
        [self calculateConversion:1];
        
    }else if(_sgmSelector.selectedSegmentIndex == 2){
        
        [self setMinMax:32.00 :212.00];
        
        [self calculateConversion:2];
        
    }else if(_sgmSelector.selectedSegmentIndex == 3){
        
        [self setMinMax:273.15 :373.15];

        [self calculateConversion:3];
        
    }else if(_sgmSelector.selectedSegmentIndex == 4){
        
        [self setMinMax:273.15 :373.15];
        
        [self calculateConversion:4];
        
        
    }else if(_sgmSelector.selectedSegmentIndex == 5){
        
        [self setMinMax:0.00 :100.00];
        
        [self calculateConversion:5];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sldSelectTemp:(id)sender {
    if(_sgmSelector.selectedSegmentIndex == 0){
        
        [self calculateConversion:0];
        
    }else if(_sgmSelector.selectedSegmentIndex == 1){

        [self calculateConversion:1];
        
    }else if(_sgmSelector.selectedSegmentIndex == 2){

        [self calculateConversion:2];
        
    }else if(_sgmSelector.selectedSegmentIndex == 3){

        [self calculateConversion:3];
        
    }else if(_sgmSelector.selectedSegmentIndex == 4){

        [self calculateConversion:4];
        
    }else if(_sgmSelector.selectedSegmentIndex == 5){

        [self calculateConversion:5];
        
    }

}
-(void)setMinMax:(float)min :(float)max{
    self.sldTemp.minimumValue = min;
    self.sldTemp.maximumValue = max;
    self.lblMin.text = [NSString stringWithFormat:@"%.2f", (float)min];
    self.lblMax.text = [NSString stringWithFormat:@"%.2f", (float)max];
    self.sldTemp.value = (min+max)/2;
    
}

-(void)calculateConversion:(int)selection{
    if(selection == 0){
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Fahrenheit", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Celsius", ((self.sldTemp.value - 32) / 1.8)];
        
    }else if(selection == 1){
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Celsius", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Fahrenheit", ((self.sldTemp.value * 1.8) + 32)];
        
    }else if(selection == 2){
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Fahrenheit", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Kelvin", ((self.sldTemp.value + 459.67) * .55555)];
        
    }else if(selection == 3){
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Kelvin", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Fahrenheit", (1.8 *(self.sldTemp.value - 273.15) + 32)];
        
    }else if (selection == 4) {
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Kelvin", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Celsius", (self.sldTemp.value - 273.15)];
        
    } else if (selection == 5) {
        self.lblSliderValue.text = [NSString stringWithFormat:@"%.2f Celsius", self.sldTemp.value];
        self.lblConversionResult.text = [NSString stringWithFormat:@"%.2f Kelvin", (self.sldTemp.value + 273.15)];
        
    }



}







@end
